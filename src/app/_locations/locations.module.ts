import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDividerModule } from '@angular/material/divider';
import { StarRatingModule } from 'angular-star-rating';
import { RestaurantCardComponent } from './components/restaurant-card/restaurant-card.component';
import { ListContainer } from './containers/list/list.container';
import { RestaurantListContainer } from './containers/restaurant-list/restaurant-list.container';
import { LocationsRoutingModule } from './locations-routing.module';
import { LocationsView } from './locations.view';
import { FiltersContainer } from './containers/filters/filters.container';



const angularMaterialModules = [MatCardModule, MatChipsModule, MatDividerModule];



@NgModule({
    declarations: [
        LocationsView,
        ListContainer,
        RestaurantListContainer,
        RestaurantCardComponent,
        FiltersContainer
    ],
    imports: [
        CommonModule,
        LocationsRoutingModule,
        StarRatingModule.forChild(),
        ...angularMaterialModules
    ]
})
export class LocationsModule { }

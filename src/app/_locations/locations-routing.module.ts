import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListContainer } from './containers/list/list.container';
import { RestaurantListContainer } from './containers/restaurant-list/restaurant-list.container';
import { LocationsView } from './locations.view';
import { PostCodeListResolver } from './resolvers/post-code-list.resolver';



export enum LocationsRouterNames {
    LIST = '',
    POST_CODE = 'post-code'
}



const routes: Routes = [

    {
        path: '',
        component: LocationsView,
        children: [

            {
                path: LocationsRouterNames.LIST,
                component: ListContainer,
            },

            {
                path: `:${LocationsRouterNames.POST_CODE}`,
                component: RestaurantListContainer,
                resolve: {
                    postCodeList: PostCodeListResolver
                }
            }
        ]
    }

];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LocationsRoutingModule { }

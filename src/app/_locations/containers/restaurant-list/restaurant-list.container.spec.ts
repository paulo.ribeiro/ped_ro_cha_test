import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantListContainer } from './restaurant-list.container';

describe('RestaurantListContainer', () => {
  let component: RestaurantListContainer;
  let fixture: ComponentFixture<RestaurantListContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestaurantListContainer ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RestaurantListContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

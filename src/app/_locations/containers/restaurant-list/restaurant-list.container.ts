import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPostCode } from 'src/app/models/post-code.model';



@Component({
    selector: 'app-restaurant-list',
    templateUrl: './restaurant-list.container.html',
    styleUrls: ['./restaurant-list.container.scss']
})
export class RestaurantListContainer implements OnInit {

    public postCodeList: IPostCode | null = null;


    constructor(private activatedRoute: ActivatedRoute) { }


    ngOnInit(): void {
        this.activatedRoute
            .data
            .subscribe(({ postCodeList }) => this.postCodeList = postCodeList)
    }

}

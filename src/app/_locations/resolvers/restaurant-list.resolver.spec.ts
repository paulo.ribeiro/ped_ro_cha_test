import { TestBed } from '@angular/core/testing';

import { PostCodeListResolver } from './post-code-list.resolver';

describe('RestaurantListResolver', () => {
  let resolver: PostCodeListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(PostCodeListResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot, Resolve,
    RouterStateSnapshot
} from '@angular/router';
import { LocationsRouterNames } from '@locations/locations-routing.module';
import { Observable } from 'rxjs';
import { IPostCode } from 'src/app/models/post-code.model';
import { RestaurantService } from 'src/app/services/restaurant.service';



@Injectable({
    providedIn: 'root'
})
export class PostCodeListResolver implements Resolve<IPostCode> {

    constructor(private restaurantService: RestaurantService) { }


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPostCode> {
        const postCode: string = route.paramMap.get(LocationsRouterNames.POST_CODE) as string;
        return this.restaurantService.getByPostCode(postCode);
    }

}

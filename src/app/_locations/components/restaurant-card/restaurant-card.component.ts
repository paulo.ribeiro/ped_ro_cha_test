import { Component, Input, OnInit } from '@angular/core';
import { IRestaurant } from 'src/app/models/restaurant.model';



@Component({
    selector: 'location-restaurant-card',
    templateUrl: './restaurant-card.component.html',
    styleUrls: ['./restaurant-card.component.scss']
})
export class RestaurantCardComponent implements OnInit {
    @Input() public restaurant: IRestaurant | null = null;

    constructor() { }

    ngOnInit(): void {
    }

}

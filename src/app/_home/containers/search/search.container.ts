import { Component } from '@angular/core';
import { Router } from '@angular/router';



@Component({
    selector: 'home-search',
    templateUrl: './search.container.html',
    styleUrls: ['./search.container.scss']
})
export class SearchContainer {

    constructor(private router: Router) { }


    public searchByPostCode({ target }: Event): void {
        const { value } = (target as HTMLInputElement);
        this.router.navigate([`locations/${value}`]);
    }

}

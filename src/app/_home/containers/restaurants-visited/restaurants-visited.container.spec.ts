import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantsVisitedContainer } from './restaurants-visited.container';

describe('RestaurantsVisitedContainer', () => {
  let component: RestaurantsVisitedContainer;
  let fixture: ComponentFixture<RestaurantsVisitedContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestaurantsVisitedContainer ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RestaurantsVisitedContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCodesVisitedContainer } from './post-codes-visited.container';

describe('PostCodesVisitedContainer', () => {
  let component: PostCodesVisitedContainer;
  let fixture: ComponentFixture<PostCodesVisitedContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostCodesVisitedContainer ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PostCodesVisitedContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

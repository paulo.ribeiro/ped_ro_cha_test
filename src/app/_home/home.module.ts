import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { HomeRoutingModule } from './home-routing.module';
import { HomeView } from './home.view';
import { SearchContainer } from './containers/search/search.container';
import { PostCodesVisitedContainer } from './containers/post-codes-visited/post-codes-visited.container';
import { RestaurantsVisitedContainer } from './containers/restaurants-visited/restaurants-visited.container';



const angularMaterialModules = [MatCardModule, MatIconModule, MatFormFieldModule, MatInputModule]



@NgModule({
    declarations: [
        HomeView,
        SearchContainer,
        PostCodesVisitedContainer,
        RestaurantsVisitedContainer
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        ...angularMaterialModules
    ]
})
export class HomeModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoreView } from './core.view';

describe('CoreView', () => {
  let component: CoreView;
  let fixture: ComponentFixture<CoreView>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoreView ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoreView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

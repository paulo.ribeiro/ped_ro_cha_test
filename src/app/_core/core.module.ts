import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TopNavContainer } from './containers/top-nav/top-nav.container';
import { CoreRoutingModule } from './core-routing.module';
import { CoreView } from './core.view';



const angularMaterialModules = [MatToolbarModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule]



@NgModule({
    declarations: [
        CoreView,
        TopNavContainer
    ],
    imports: [
        CommonModule,
        CoreRoutingModule,
        FormsModule,
        ...angularMaterialModules
    ]
})
export class CoreModule { }

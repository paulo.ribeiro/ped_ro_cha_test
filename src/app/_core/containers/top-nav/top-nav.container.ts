import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';



@Component({
    selector: 'core-top-nav',
    templateUrl: './top-nav.container.html',
    styleUrls: ['./top-nav.container.scss']
})
export class TopNavContainer {

    public showSearchInput: boolean = false;
    public postCode: string = '';

    constructor(private router: Router) {
        router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event) => this.showSearchInput = (event as NavigationEnd).url !== '/');

    }


    public searchByPostCode(): void {
        this.router.navigate([`locations/${this.postCode}`]);
    }

}

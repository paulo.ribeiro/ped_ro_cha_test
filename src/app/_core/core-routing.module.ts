import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoreView } from './core.view';



enum CoreRouterNames {
    HOME = '',
    LOCATIONS = 'locations'
}



const routes: Routes = [

    {
        path: '',
        component: CoreView,
        children: [

            {
                path: CoreRouterNames.HOME,
                loadChildren: () => import('@home/home.module').then(m => m.HomeModule)
            },

            {
                path: CoreRouterNames.LOCATIONS,
                loadChildren: () => import('@locations/locations.module').then(m => m.LocationsModule)
            }

        ]
    }

];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoreRoutingModule { }

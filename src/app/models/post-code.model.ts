import { IRestaurant } from "./restaurant.model";



export interface IPostCode {
    Area: string;
    CuisineSets: any[];
    Dishes: any[];
    MetaData: IMetaData;
    RestaurantSets: any[]
    Restaurants: IRestaurant[];
    ShortResultText: string;
    Views: any[];
    deliveryFees: any;
    promotedPlacement: any;
}



interface IMetaData {
    Area: string;
    CanonicalName: string;
    CollectionExperimentInjectedRestaurantIds: any;
    CuisineDetails: any[];
    DeliveryArea: string;
    District: string;
    Latitude: number;
    Longitude: number;
    Postcode: string;
    ResultCount: number;
    SearchedTerms: any;
    TagDetails: any
}

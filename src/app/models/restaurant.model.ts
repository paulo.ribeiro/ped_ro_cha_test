export interface IRestaurant {
    Address: IAddress;
    Badges: any[];
    BrandName: string;
    City: string;
    CollectionMenuId: any;
    CuisineTypes: any[];
    Cuisines: any[];
    Deals: any[];
    DefaultDisplayRank: number;
    DeliveryChargeBands: any[];
    DeliveryCost: number;
    DeliveryEtaMinutes: IDeliveryEtaMinutes;
    DeliveryMenuId: null
    DeliveryOpeningTime: string;
    DeliveryOpeningTimeLocal: string;
    DeliveryOpeningTimeUtc: string;
    DeliveryStartTime: string;
    DeliveryTime: string;
    DeliveryTimeMinutes: string;
    DeliveryWorkingTimeMinutes: number;
    DeliveryZipcode: string;
    Description: string;
    DriveDistance: number;
    DriveInfoCalculated: boolean;
    HygieneRating: any;
    Id: number;
    IsBrand: boolean;
    IsCloseBy: boolean;
    IsCollection: boolean;
    IsDelivery: boolean;
    IsFreeDelivery: boolean;
    IsHalal: boolean;
    IsNew: boolean;
    IsOpenNow: boolean;
    IsOpenNowForCollection: boolean;
    IsOpenNowForDelivery: boolean;
    IsOpenNowForPreorder: boolean;
    IsPremier: boolean;
    IsSponsored: boolean;
    IsTemporarilyOffline: boolean;
    IsTemporaryBoost: boolean;
    IsTestRestaurant: boolean;
    LastUpdated: string;
    Latitude: number;
    Logo: any[]
    LogoUrl: string;
    Longitude: number;
    MinimumDeliveryValue: number;
    Name: string;
    NewnessDate: string;
    NumberOfRatings: number;
    OfferPercent: number;
    Offers: any[]
    OpeningTime: string;
    OpeningTimeIso: string;
    OpeningTimeLocal: string;
    OpeningTimeUtc: null
    OpeningTimes: any[]
    Postcode: string;
    Rating: IRating;
    RatingAverage: number;
    RatingStars: number;
    ReasonWhyTemporarilyOffline: string;
    Score: number;
    ScoreMetaData: any[];
    SecondDateRank: number;
    SecondDateRanking: number;
    SendsOnItsWayNotifications: boolean;
    ServiceableAreas: any[];
    ShowSmiley: boolean;
    SmileyDate: any;
    SmileyElite: boolean;
    SmileyResult: any;
    SmileyUrl: any;
    SponsoredPosition: number;
    Tags: string[];
    UniqueName: string;
    Url: string;
}

interface IAddress {
    City: string;
    FirstLine: string;
    Postcode: string;
    Latitude: number;
    Longitude: number;
}

interface IDeliveryEtaMinutes { Approximate: any; RangeLower: number; RangeUpper: number; }

interface IRating { Count: number; Average: number; StarRating: number; }
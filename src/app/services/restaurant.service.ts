import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { IPostCode } from '../models/post-code.model';



@Injectable({
    providedIn: 'root'
})
export class RestaurantService {

    constructor(private http: HttpClient) { }


    public getByPostCode(postCode: string): Observable<IPostCode> {
        return this.http.get<IPostCode>(`https://uk.api.just-eat.io/restaurants/bypostcode/${postCode}`);
    }

}

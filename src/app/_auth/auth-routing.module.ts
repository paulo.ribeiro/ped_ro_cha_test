import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthView } from './auth.view';



const routes: Routes = [

    {
        path: '',
        component: AuthView
    }

];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }

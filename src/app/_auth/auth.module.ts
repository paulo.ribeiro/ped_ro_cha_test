import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthView } from './auth.view';



@NgModule({
    declarations: [
    
    AuthView
  ],
    imports: [
        CommonModule,
        AuthRoutingModule
    ]
})
export class AuthModule { }
